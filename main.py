from flask import Flask,request,jsonify,abort
import psycopg2

con = psycopg2.connect("user='postgres' dbname='greenhouse_gases' password=''")
cur = con.cursor()

app = Flask(__name__)

@app.errorhandler(500)
def internal_error(error):
  return jsonify({"error":"Internal server error"})

@app.route("/")
def Test():
  return "<p>Testing:true</p>"

@app.route("/countries")
def get_countries():

  response = []

  sql_query = "Select countries.country_name,greenhouse_data.country_id,Min(greenhouse_data.year) as start_year,Max(greenhouse_data.year) as end_year From countries,greenhouse_data Where greenhouse_data.country_id = countries.id group by greenhouse_data.country_id , countries.country_name;"
  
  try:
    cur.execute(sql_query)
    get_data = cur.fetchall()
  except Exception :
    abort(500)

  for data in get_data:
    result = {
      "country_name" : data[0],
      "country_id"   : data[1],
      "start_year"   : data[2],
      "end_year"     : data[3]
    }
    response.append(result)
  return jsonify(response)

@app.route("/countries/<id>")
def get_country_data(id):

  response = []
  start_year_where_clause = ""
  end_year_where_clause = ""
  gas_where_clause = ""

  start_year = request.args.get('start_year')
  end_year   = request.args.get('end_year')
  emission_category = request.args.get('category')
  
  if emission_category is not None :
    gas_where_clause = " and categories.category_name IN (" + emission_category + ")"

  if start_year is not None :
    end_year_where_clause = " and greenhouse_data.year >= " + start_year

  if end_year is not None :
    start_year_where_clause = " and greenhouse_data.year <= " + end_year

  sql_query = "Select countries.country_name,greenhouse_data.country_id,greenhouse_data.year,greenhouse_data.value,categories.category_name From countries LEFT JOIN greenhouse_data ON countries.id = greenhouse_data.country_id LEFT JOIN categories ON categories.id = greenhouse_data.category_id Where greenhouse_data.country_id = "+ id + start_year_where_clause + end_year_where_clause + gas_where_clause +";"

  try:
    cur.execute(sql_query)
    get_data = cur.fetchall()
  except Exception :
    abort(500)

  if not get_data:
    return jsonify({"message":"Empty response"}) 

  for data in get_data:
  
    result = {
      "country_name" : data[0],
      "country_id"   : data[1],
      "year"         : data[2],
      "value"        : data[3],
      "category"     : data[4]
    }
    response.append(result)
    
  return jsonify(response)

if __name__ == '__main__':
  app.run(debug = True)