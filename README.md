# International Greenhouse Gas Emissions

Dataset: https://www.kaggle.com/unitednations/international-greenhouse-gas-emissions

## Cleaning Data

1. Shortening Greenhouse gases name in "category" as:
    * carbon_dioxide_co2_emissions_without_land_use_land_use_change_and_forestry_lulucf_in_kilotonne_co2_equivalent - CO2 
	* greenhouse_gas_ghgs_emissions_including_indirect_co2_without_lulucf_in_kilotonne_co2_equivalent - GHG-indirect-CO2
	* greenhouse_gas_ghgs_emissions_without_land_use_land_use_change_and_forestry_lulucf_in_kilotonne_co2_equivalent - GHG
	* hydrofluorocarbons_hfcs_emissions_in_kilotonne_co2_equivalent - HFC
	* methane_ch4_emissions_without_land_use_land_use_change_and_forestry_lulucf_in_kilotonne_co2_equivalent - CH4
	* nitrogen_trifluoride_nf3_emissions_in_kilotonne_co2_equivalent - HF3
	* nitrous_oxide_n2o_emissions_without_land_use_land_use_change_and_forestry_lulucf_in_kilotonne_co2_equivalent - N2Os
	* perfluorocarbons_pfcs_emissions_in_kilotonne_co2_equivalent - PFCs
	* sulphur_hexafluoride_sf6_emissions_in_kilotonne_co2_equivalent - F6
	* unspecified_mix_of_hydrofluorocarbons_hfcs_and_perfluorocarbons_pfcs_emissions_in_kilotonne_co2_equivalent - HFC-PFC-mix

2. Ignore values under "country_or_area" which does not refer to a country name
    * Example: Ignore "European Union", "Russian Federation" because they are not countries.

## API Design

1. /countries - get all countries with names, ids and their values for startYear and endYear
   * Returns json 
   * For Example : [
    {
			"country_id": 21,
			"country_name": "Slovakia",
			"end_year": 2014,
			"start_year": 1990
    },
    {
			"country_id": 37,
			"country_name": "Hungary",
			"end_year": 2014,
			"start_year": 1990
    }]

2. /countries/id?queries=
	 1. For temporal queries - startYear | endYear 
		* End Point - /countries/2?start_year=2008&end_year=2014
		* Returns json with start_year = 2008 , end_year = 2014 and id(country_id) = 2 

	 2. Parameters queries - one or parameters -(e.g, CO2 or CO2 and NO2)
		* End Point - /countries/2?category='CO2','HFC'
		* Returns json with category as CO2 , HFC

# How To Setup The project On Local
* 1. Clone the project 
* 2. Install Postgres database : https://www.postgresql.org/download/linux/ubuntu/
* 3. Create database greenhouse_gases
* 4. Install packages by : pip install -r requirements.txt
* 5. Run the script by : python load_data.py
* 6. start the Flask Server : python main.py 