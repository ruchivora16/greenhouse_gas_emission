
import psycopg2
import pandas as pd

ghg_map = {
	"carbon_dioxide_co2_emissions_without_land_use_land_use_change_and_forestry_lulucf_in_kilotonne_co2_equivalent":"CO2",
	"greenhouse_gas_ghgs_emissions_including_indirect_co2_without_lulucf_in_kilotonne_co2_equivalent":"GHG-indirect-CO2",
	"greenhouse_gas_ghgs_emissions_without_land_use_land_use_change_and_forestry_lulucf_in_kilotonne_co2_equivalent":"GHG",
	"hydrofluorocarbons_hfcs_emissions_in_kilotonne_co2_equivalent":"HFC",
	"methane_ch4_emissions_without_land_use_land_use_change_and_forestry_lulucf_in_kilotonne_co2_equivalent":"CH4",
	"nitrogen_trifluoride_nf3_emissions_in_kilotonne_co2_equivalent":"HF3",
	"nitrous_oxide_n2o_emissions_without_land_use_land_use_change_and_forestry_lulucf_in_kilotonne_co2_equivalent":"N2Os",
	"perfluorocarbons_pfcs_emissions_in_kilotonne_co2_equivalent":"PFCs",
	"sulphur_hexafluoride_sf6_emissions_in_kilotonne_co2_equivalent":"F6",
	"unspecified_mix_of_hydrofluorocarbons_hfcs_and_perfluorocarbons_pfcs_emissions_in_kilotonne_co2_equivalent":"HFC-PFC-mix"
}

con = psycopg2.connect("user='postgres' dbname='greenhouse_gases' password=''")
cur = con.cursor()


drop_table_ghg_inventory = "DROP TABLE IF EXISTS ghg_inventory"
cur.execute(drop_table_ghg_inventory)

drop_table_greenhouse_data = "DROP TABLE IF EXISTS greenhouse_data;"
cur.execute(drop_table_greenhouse_data)

drop_table_countries = "DROP TABLE IF EXISTS countries;"
cur.execute(drop_table_countries)

drop_table_categories = "DROP TABLE IF EXISTS categories;"
cur.execute(drop_table_categories)

create_table_countries = "CREATE TABLE countries( id serial PRIMARY KEY,country_name varchar(255) NOT NULL);"
cur.execute(create_table_countries)

create_table_categories = "CREATE TABLE categories( id serial PRIMARY KEY,category_name varchar(255) NOT NULL);"
cur.execute(create_table_categories)

create_table_greenhouse_data = "CREATE TABLE greenhouse_data ( id serial PRIMARY KEY,country_id int NOT NULL,year int NOT NULL,value Float(20) NOT NULL,category_id int NOT NULL,FOREIGN KEY (country_id) REFERENCES countries(id),FOREIGN KEY (category_id) REFERENCES categories(id));"
cur.execute(create_table_greenhouse_data)

create_table_ghg_inventory = "CREATE TABLE ghg_inventory (country_or_area varchar(255),year int,value Float(20),category varchar(255));"
cur.execute(create_table_ghg_inventory)

country_set  = set()
category_set = set()
list_of_area = ['European Union','Russian Federation']

try:
	ghg_inventory_data = pd.read_csv('greenhouse_gas_inventory_data_data.csv')

	## Populate ghg_inventory_data table
	for index,row in ghg_inventory_data.iterrows():

		country_or_area = row['country_or_area']
		year  = row['year']
		value = row['value']
		category = row['category']
		cur.execute("INSERT INTO ghg_inventory (country_or_area,year,value,category) VALUES (%s,%s,%s,%s)",(country_or_area,year,value,category))

	## Loop to clean the data
	for index,row in ghg_inventory_data.iterrows():

		if row['country_or_area'] not in country_set and row['country_or_area'] not in list_of_area:
			country_set.add(row['country_or_area'])

		ghg_name = row['category']
		ghg_name = ghg_map[ghg_name]

		if ghg_name not in category_set:
			category_set.add(ghg_name)

		ghg_inventory_data.at[index,'category'] = ghg_name

# Populate Category Table
	for category in category_set:
		cur.execute("Insert INTO categories (category_name) VALUES (%s)",(category,))

#Populate Country Table
	for country in country_set:
		cur.execute("Insert INTO countries (country_name) VALUES (%s)",(country,))

  #Polpulate greenhouse_data Table
	for index,row in ghg_inventory_data.iterrows():

		cur.execute("Select id from countries where country_name LIKE %s ESCAPE ''",(row['country_or_area'],))
		country_id  = cur.fetchone()
		if country_id is None:
			continue
		
		cur.execute("Select id from categories where category_name LIKE %s ESCAPE ''",(row['category'],))
		category_id = cur.fetchone()
		
		year   = row['year']
		value  = row['value']
		
		cur.execute("Insert INTO greenhouse_data (country_id,year,value,category_id) VALUES (%s,%s,%s,%s)",(country_id,year,value,category_id))
		
except Exception as e:
	print("Error opening greenhouse_gas_ghg_inventory_data.csv file")


con.commit()























